Title: Kresus - Gestionnaire de finances personnelles
Date: 2017-03-11 10:02
Slug: home
Author: Nicolas Frandeboeuf
Summary: Home
Save_as: index.html
Status: hidden

## Qu'est-ce que Kresus ?

<div id="homedesc">
    <img src="/theme/images/logo.png">

    <p>
        Kresus est un gestionnaire de finances personnelles qui vous permet de suivre l'état de vos
        finances, en important tous <strong>les comptes de toutes vos banques à un seul
        endroit</strong>. Des <strong>alertes emails</strong> sur montant d'une transaction ou sur
        solde peuvent être configurées par vos soins et vous prévenir en cas d'événements importants
        sur vos comptes. La possibilité de créer des <strong>catégories</strong> et ensuite
        d'attribuer ces catégories à vos comptes vous donne les réponses à des questions telles que
        "<em>Quel est mon plus gros poste de dépenses ce mois-ci ?</em>" ou encore "<em>Comment se
        comparent mes rentrées d'argent au cours des six derniers mois ?</em>".
    </p>

    <p class="download">
        <a class="button" href="install.html">Installer Kresus</a>
    </p>
</div>

<div class="features">
    <div>
        <a class="screenshot" href="images/pages/framagit.png">
            <img alt="Capture écran Framagit" src="images/pages/framagit.png">
        </a>

        <h3>Libre, auto-hébergeable et personnel</h3>
        <p>
            Les données bancaires sont extrêmement personnelles, et de valeur très élevée pour
            toutes les corporations, car elles traduisent tous nos actes de consommation,
            révélateurs de qui nous sommes, où nous nous trouvons et de ce que nous soutenons, qui
            nous affecte, qui nous touche.
        </p>

        <p>
            Par ailleurs, qui se sentirait à l'aise à l'idée de partager les identifiants et mots de
            passe de services bancaires en ligne à des parfaits inconnus&nbsp;?
        </p>

        <p>
            Pour éviter cet écueil, Kresus est un logiciel auto-hébergeable, libre et open-source,
            que vous pouvez modifier et partager à volonté, et également installer très facilement
            sur votre serveur personnel. Kresus repose sur les épaules d'un autre projet libre et
            open-source très actif, <a href="http://weboob.org/">Weboob</a>, qui fait le lien entre
            le site bancaire et Kresus. Ainsi, vous avez le contrôle total sur le logiciel et ce
            qu'il fait de vos données.
        </p>
    </div>
</div>

<div class="features">
    <div>
        <a class="screenshot" href="images/pages/view-all-accounts.png">
            <img alt="Capture écran vue globale" src="images/pages/view-all-accounts.png">
        </a>

        <div>
            <h3>Vue d'ensemble de tous vos comptes</h3>
            <p>
                Retrouvez le solde de chacun de vos comptes rapidement, ainsi que le total pour
                chaque banque&nbsp;!
            </p>
        </div>
    </div>
</div>

<div class="features">
    <div>
        <a class="screenshot" href="images/pages/budget.png">
            <img alt="Capture écran évolution mois" src="images/pages/budget.png">
        </a>

        <div>
            <h3>Suivez l'évolution de vos rentrées et sorties</h3>
            <p>
                En un coup d'œil, vérifiez vos rentrées et sorties d'argent pour le mois courant.
            </p>
        </div>
    </div>
</div>

<div class="features">
    <div>
        <a class="screenshot" href="images/pages/categories.png">
            <img alt="Capture écran catégories" src="images/pages/categories.png">
        </a>

        <div>
            <h3>Classifiez, renommez, recherchez vos opérations</h3>
            <ul>
                <li>Définissez vos propres labels, finis les "Chèque n°168468"&nbsp;!</li>
                <li>Classifiez vos opérations dans des catégories</li>
                <li>Effectuez des recherches complexes sur vos transactions, par période, catégorie,
                montant ou date !</li>
            </ul>
        </div>
    </div>
</div>

<div class="features">
    <div>
        <a class="screenshot" href="images/pages/charts.png">
            <img alt="Capture écran graphiques" src="images/pages/charts.png">
        </a>

        <div>
            <h3>Graphiques</h3>
            <p>
                Suivez l'évolution des vos dépenses/rentrées ou de votre solde, en un clin d'œil via
                les graphiques.
                <br>Vous pouvez également cibler :
            </p>
            <ul>
                <li>des catégories spécifiques</li>
                <li>une période (mois courant, mois précédent, 3 derniers mois, 6 derniers mois,
                depuis le début)</li>
                <li>les dépenses ou rentrées uniquement</li>
            </ul>
        </div>
    </div>
</div>

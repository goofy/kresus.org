Title: Contribuer
Date: 2017-03-11 10:02
Slug: contribute
Author: Nicolas Frandeboeuf
Summary: Comment contribuer à Kresus

# Comment Contribuer

Vous souhaitez contribuer à Kresus ?
Voici plusieurs possibilités :

* Participez au développement
* Améliorez le design
* Traduisez Kresus dans d'autres langues
* Rédigez des documentations (ce site est libre aussi : [https://framagit.org/bnjbvr/kresus.org](https://framagit.org/bnjbvr/kresus.org) !)
* Parlez-en autour de vous

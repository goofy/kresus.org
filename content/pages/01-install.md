Title: Installation
Date: 2017-03-11 10:02
Slug: install
Author: Nicolas Frandeboeuf
Summary: Installation

# Comment installer Kresus

## Pré-requis

Kresus utilise [Weboob](http://weboob.org/) sous le capot, pour se connecter au site web de votre banque. Vous aurez besoin d'[installer le cœur et les modules Weboob](http://weboob.org/install) afin que l'utilisateur exécutant Kresus puisse les utiliser.

Kresus nécessite la dernière version stable de Weboob. Bien que Kresus puisse fonctionner avec de précédentes versions, les modules des banques peuvent être obsolètes, et la synchronisation avec votre banque s'en retrouverait disfonctionnelle..

## Installation standalone

**AVERTISSEMENT: Il n'y a aucun système d'authentification intégré dans Kresus, il est donc risqué de l'utiliser tel quel. Choisiseez cette option uniquement si vous savez ce que vous faites et êtes capable de gérer l'authentification vous-même..**

Cela installera les dépendances, construira le projet et installera le programme dans le répertoire node.js global. Notez que si cet emplacement est `/usr/local/bin`, vous devrez probablement lancer cette commande en tant que root.

### Setup local

Installez les dépendances node et construisez les scripts (ça n'installera pas Kresus globalement) :

    :::bash
    make localrun

### Setup global

Autrement, si vous souhaitez installer Kresus globalement vous utiliserez :

    :::bash
    make install

Et pourrez ensuite simplement lancer Kresus depuis n'importe quel terminal depuis n'importe quel répertoire avec :

    :::bash
    kresus

## Avec Docker

### Lancez une image pré-construite

    :::bash
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d bnjbvr/kresus


### Construisez l'image

Il existe un Dockerfile depuis lequel vous pouvez construire et lancer Kresus, en utilisant les commandes suivantes (n'oubliez pas de modifier le mapping des ports et des volumes, si nécessaire !) :

    :::bash
    git clone https://framagit.org/bnjbvr/kresus && cd kresus
    docker build -t kresus .
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d kresus

## Installation sur CozyCloud

Si vous possédez déjà une instance Cozy, la meilleure solution (et la [seule](https://github.com/cozy/cozy-home/issues/789)) est d'installer Kresus depuis le *marché*.

# Options au lancement

Notez que quelque soit le processus de lancement choisi (global ou local), vous pouvez définier plusieurs options :

- le **port** par défaut est 9876. Ça peut être surchargé via la variable d'environnement `PORT`.

- le **host** par défaut sur lequel Kresus écoute est `localhost`. Ça peut être surchargé via la variable d'environnement `HOST`.

- en mode standalone, le répertoire d'installation par défaut est  `~/.kresus/`. Ça peut être surchargé via la variable d'environnement `KRESUS_DIR`.

## Recommendations pour le pare-feu

Vous devrez définir les autorisations suivantes :

- Accès http/https au site web de votre banque, pour récupérer les nouvelles opérations.
- Accès http/https aux dépôts Weboob, pour la mise à jour automatique des modules avant les rappatriements automatiques.
